namespace Deck;

interface IQueue<T>: IEnumerable<T>, ICloneable
{
    public void AddItem(T item); // метод добавления нового элемента в очередь 
    public void RemoveItem(T item); // метод удаления элемента из очереди 
    public T ReturnFirstItem(); // метод возврата первого элемента
    public T ReturnLastItem(); // метод возврата последнего элемента
}