using System.Collections;

namespace Deck;

[Serializable]
public class StackOnArr<T> : IStack<T>, ICloneable
{
    private T[] items;
    private int count;
    const int n = 10;
    public StackOnArr()
    {
        items = new T[n];
    }
    public StackOnArr(int length)
    {
        items = new T[length];
    }
    public StackOnArr(StackOnArr<T> stack)
    {

        this.items = stack.items;
        this.count = stack.count;
    }

    public bool IsEmpty // проверка на пустоту стека
    {
        get { return count == 0; }
    }
    public int Count // возвращение кол-ва элементов стека
    {
        get { return count; }
    }

    public void Push(T item) // добавление элемента в стек
    {
        // увеличиваем стек
        if (count == items.Length)
            Resize(items.Length + 10);

        items[count++] = item;
    }
    public T Pop() // удаление элемента из стека
    {
        // если стек пуст, выбрасываем исключение
        if (IsEmpty)
            throw new InvalidOperationException("Стек пуст");
        T item = items[--count];
        items[count] = default(T); // сбрасываем ссылку

        if (count > 0 && count < items.Length - 10)
            Resize(items.Length - 10);

        return item;
    }
    public T Peek() // возврат последнего элемента
    {
        return items[count - 1];
    }

    private void Resize(int max) // метод для расширения массива стека
    {
        T[] tempItems = new T[max];
        for (int i = 0; i < count; i++)
            tempItems[i] = items[i];
        items = tempItems;
    }


    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < count; i++)
            yield return this.Pop(); // yield используется для корректной работы вывода элемента
    }
    
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    // Реализация глубокого копирования (Создается новый экземпляр копируемого объекта, в отличии от Memberwiseclone где копируется ссылка на объект и значения)
    public object Clone()
    {
        return new StackOnArr<T>(this);
    }

    // для стека с типом данных int
    public static double GetArithMeanPipeLine(StackOnArr<int> stack)
    {
        try { return stack.Where(x => x > 0).Average(); }
        catch { return 0; }
    }

    public static double GetArithMean(StackOnArr<int> stack)
    {
        int count = 0;
        int sum = 0;
        foreach (int item in stack)
        {
            if (item > 0)
            {
                sum += item;
                count++;
            }
        }
        return sum / count;
    }
    // для стека с типом данных double
    public static double GetArithMeanPipeLine(StackOnArr<double> stack)
    {
        try { return stack.Where(x => x > 0).Average(); }
        catch { return 0; }
    }

    public static double GetArithMean(StackOnArr<double> stack)
    {
        int count = 0;
        int sum = 0;
        foreach (int item in stack)
        {
            if (item > 0)
            {
                sum += item;
                count++;
            }
        }
        return sum / count;
    }

}
