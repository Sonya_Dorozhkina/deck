namespace Deck;

public interface IDeck<T> : IEnumerable<T>, ICloneable
{
    public void AddItem(T item); // метод добавления нового элемента в очередь 
    public T RemoveItem(); // метод удаления элемента из очереди 
}