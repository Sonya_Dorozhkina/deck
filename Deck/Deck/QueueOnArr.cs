namespace Deck;

using System.Collections;

[Serializable]
public class QueueOnArr<T> : IEnumerable
{
    private T[] items = new T[10];
    private int count;
    const int n = 10;
    public QueueOnArr() // конструктор класса Queue создающий пустую очередь
    {
        items = new T[n];
    }
    public QueueOnArr(int length) // конструктор класса Queue создающий пустую очередь заданной длинны
    {
        items = new T[length];
    }
    public QueueOnArr(T[] items) // конструктор класса Queue создающий очередь с элементами из массива
    {
        for (int i = 0; i < items.Length; i++)
        {
            this.items[i] = items[i];
        }
        count = items.Length;
    }
    public QueueOnArr(QueueOnArr<T> queue) // конструктор класса Queue создающий очередь
    {
        this.items = queue.items;
        this.count = queue.count;
    }
    public bool IsEmpty // проверка на пустоту стека
    {
        get { return count == 0; }
    }
    public int Count // возвращение кол-ва элементов стека
    {
        get { return count; }
    }
    private void Resize(int max) // метод для расширения массива стека
    {
        T[] tempItems = new T[max];
        for (int i = 0; i < count; i++)
            tempItems[i] = items[i];
        items = tempItems;
    }
    public void AddItem(T item) // добавить элемент в очередь
    {
        // увеличиваем стек
        if (count == items.Length)
            Resize(items.Length + 10);

        items[count++] = item;
    }
    public T RemoveItem() // удалить из очереди элемент и его возврат
    {
        // если стек пуст, выбрасываем исключение
        if (IsEmpty)
            throw new InvalidOperationException("очередь пуста");
        else { 
            T item = items[0];

            for (int i = 0; i < items.Length - 1; i++)
                items[i] = items[i + 1];

            items[items.Length - 1] = default(T);

            if (count > 0 && count < items.Length - 10)
                Resize(items.Length - 10);

            count--; 
            return item;
        }
    }
    public T ReturnFirstItem() // возврат первого элемента
    {
        return items[0];
    }
    public T ReturnLastItem() // возврат последнего элемента
    {
        return items[count - 1];
    }


    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < count; i++)
            yield return this.items[i];
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    // Реализация глубокого копирования (Создается новый экземпляр копируемого объекта, в отличии от Memberwiseclone где копируется ссылка на объект и значения)
    public object Clone()
    {
        return new QueueOnArr<T>(this);
    }
}