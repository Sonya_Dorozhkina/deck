using System.Collections;

namespace Deck;

[Serializable]
public class StackOnLikedList<T> : IStack<T>, ICloneable
{
    private LinkedList<T> items;
    public StackOnLikedList()
    {
        items = new LinkedList<T>();
    }
    public StackOnLikedList(StackOnLikedList<T> stack)
    {

        this.items = stack.items;
    }
    public bool IsEmpty // проверка на пустоту стека
    {
        get { return items.Count == 0; }
    }
    public int Count // возвращение кол-ва элементов стека
    {
        get { return items.Count; }
    }
    public void Push(T item) // добавление элемента в стек
    {
        items.AddLast(item);
    }
    public T Pop() // удаление элемента из стека
    {
        // если стек пуст, выбрасываем исключение
        if (IsEmpty)
            throw new InvalidOperationException("Стек пуст");

        LinkedListNode<T> item = items.Last;
        items.RemoveLast();
        return item.Value;
    }
    public T Peek() // возврат последнего элемента
    {
        return items.Last.Value;
    }
    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < items.Count; i++)
            yield return this.Pop(); // yield используется для корректной работы вывода элемента
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
    // Реализация клонирования (Создается новый экземпляр копируемого объекта, который копирует ссылку на объект и значения)
    public object Clone()
    {
        return new StackOnLikedList<T>(this);
    }
    // Работа с анонимными функциями (6ая лаба)
    // для стека с типом данных int
    public static double GetArithMeanPipeLine(StackOnLikedList<int> stack)
    {
        try { return stack.Where(x => x > 0).Average(); }
        catch { return 0; }
    }
    public static double GetArithMean(StackOnLikedList<int> stack)
    {
        int count = 0;
        int sum = 0;
        foreach (int item in stack)
        {
            if (item > 0)
            {
                sum += item;
                count++;
            }
        }
        return sum / count;
    }
    // для стека с типом данных double
    public static double GetArithMeanPipeLine(StackOnLikedList<double> stack)
    {
        try { return stack.Where(x => x > 0).Average(); }
        catch { return 0; }
    }
    public static double GetArithMean(StackOnLikedList<double> stack)
    {
        int count = 0;
        int sum = 0;
        foreach (int item in stack)
        {
            if (item > 0)
            {
                sum += item;
                count++;
            }
        }
        return sum / count;
    }
}