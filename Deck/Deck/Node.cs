namespace Deck;

public class Node<T>
{
    private Node<T> previous;
    private Node<T> next;
    public T content;
    public Node(T item)
    {
        content = item;
    }
}