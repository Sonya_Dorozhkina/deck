﻿using System.Collections;

namespace Deck;

[Serializable]
public class QueueOnList<T> : IQueue<T>
{
    private List<T> items = new();
    private int count;
    
    public QueueOnList() // конструктор класса Queue создающий пустую очередь
    {
        items = new();
    }
    
    public QueueOnList(QueueOnList<T> queue) // конструктор класса Queue создающий очередь
    {
        this.items = queue.items;
        this.count = queue.count;
    }
    
    public bool IsEmpty // проверка на пустоту стека
    {
        get { return count == 0; }
    }
    
    public int Count // возвращение кол-ва элементов стека
    {
        get { return count; }
    }

    public void AddToTop(T item)
    {
        for (int i = items.Count; i > 0; i--)
            items[i] = items[i + 1];
        items[0] = item;
    }
    
    public void AddToEnd(T item)
    {
        items.Add(item);
    }
    
    public void AddItem(T item) // добавить элемент в очередь
    {
        items.Add(item);
    }

    public void RemoveItem(T item) // удалить из очереди заданный элемент
    {
        if (IsEmpty)
            throw new InvalidOperationException("очередь пуста");
        else
            items.Remove(item);
    }
    
    public T ReturnFirstItem() // возврат первого элемента
    {
        return items[0];
    }
    
    public T ReturnLastItem() // возврат последнего элемента
    {
        return items[count - 1];
    }

    public IEnumerator<T> GetEnumerator()
    {
        for (int i = 0; i < count; i++)
            yield return this.items[i];
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    // Реализация глубокого копирования (Создается новый экземпляр копируемого объекта, в отличии от Memberwiseclone где копируется ссылка на объект и значения)
    public object Clone()
    {
        return new QueueOnList<T>(this);
    }
}